//interface pizzaSlice
export enum EStatus {
    LOADING = 'loading',
    SUCCESS = 'success',
    ERROR = 'error'
}

export type IPizzaArr = {
    id: string;
    title: string;
    description:string;
    price: number;
    imageUrl: string;
    sizes: number[];
    types: number[];
    rating: number;
}


export interface IPizzaSliceState {
    items: IPizzaArr[]
    status: EStatus
}

export const initialStatePizza: IPizzaSliceState = {
    items: [],
    status: EStatus.LOADING
}

export type TSearchPizzaParams = {
    sortBy: string,
    order: string,
    category: string,
    search: string,
    currentPage: string
}
