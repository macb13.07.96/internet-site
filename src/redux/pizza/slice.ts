import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {initialStatePizza, IPizzaArr, EStatus} from "./types";
import {fetchPizzas} from "./asyncActions";





const pizzaSlice = createSlice({
    name: 'pizza',
    initialState: initialStatePizza,
    reducers: {
        setItems(state, action: PayloadAction<IPizzaArr[]>) {
            state.items = action.payload
        }
    },
    extraReducers: (builder) => {
        builder.addCase(fetchPizzas.pending, (state, action) => {
            state.status = EStatus.LOADING
            state.items = []
        });
        builder.addCase(fetchPizzas.fulfilled, (state, action) => {
            state.items = action.payload
            state.status = EStatus.SUCCESS
        });
        builder.addCase(fetchPizzas.rejected, (state, action) => {
            state.status = EStatus.ERROR
            state.items = []
        })
    }
})


export const {setItems} = pizzaSlice.actions
export default pizzaSlice.reducer