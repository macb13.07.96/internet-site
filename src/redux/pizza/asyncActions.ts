import {createAsyncThunk} from "@reduxjs/toolkit";
import {IPizzaArr, TSearchPizzaParams} from "./types";
import axios from "axios";
import pickBy from 'lodash/pickBy'
import identity from 'lodash/identity'

export const fetchPizzas = createAsyncThunk<IPizzaArr[], TSearchPizzaParams>(
    'pizza/fetchPizzasStatus',
    async (params) => {
        const {sortBy, order, category, search, currentPage} = params
        const {data} = await axios.get<IPizzaArr[]>(
            `https://65b8c1f9b71048505a89603b.mockapi.io/items`, {
                params:pickBy(
                    {
                        page: currentPage,
                        category,
                        sortBy,
                        order,
                        search,
                    },
                    identity
                )
            }
        );
        return data;
    }
)

