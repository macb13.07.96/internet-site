import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {ICartItemsArr, initialStateCart} from "./types";
import {CalcTotalPrice} from "../../utils/calcTotalPrice";


const cartSlice = createSlice({
    name: 'cart',
    initialState: initialStateCart,
    reducers: {
        addItem(state, action:PayloadAction<ICartItemsArr>) {
            const findItem = state.items.find((obj) => obj.id === action.payload.id)
            if (findItem) {
                findItem.count ++
            } else {
                state.items.push({
                    ...action.payload,
                    count: 1,
                })
            }
            state.totalPrice = CalcTotalPrice(state.items)
        },
        minusItem(state, action:PayloadAction<string>){
            const findItem = state.items.find((obj) => obj.id === action.payload)
            if(findItem){
                findItem.count --
            }
            state.totalPrice = CalcTotalPrice(state.items)
        },
        removeItem(state, action:PayloadAction<string>) {
            state.items = state.items.filter((obj) => obj.id !== action.payload)
            state.totalPrice = CalcTotalPrice(state.items)
        },
        clearItems(state) {
            state.items = []
            state.totalPrice = 0
        }
    }
})


export const {addItem, removeItem, clearItems, minusItem} = cartSlice.actions
export default cartSlice.reducer