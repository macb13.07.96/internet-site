//interface cart
import {GetCartFromLS} from "../../utils/getCartFromLS";

export type ICartItemsArr = {
    id: string;
    title: string;
    price: number;
    imageUrl: string;
    type: string;
    size: number;
    count: number
}

export interface ICartSliceState {
    totalPrice: number
    items: ICartItemsArr[]

}

const {items, totalPrice} = GetCartFromLS()
export const initialStateCart: ICartSliceState = {
    totalPrice: totalPrice,
    items: items,
}
