import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {initialState, IFilterSliceState, ESortProperty} from "./types";

const filterSlice = createSlice({
    name: 'filters',
    initialState: initialState,
    reducers: {
        setCategoryId(state, action:PayloadAction<IFilterSliceState["categoryId"]>){
            state.categoryId = action.payload
        },
        setSearchValue(state, action:PayloadAction<IFilterSliceState['searchValue']>){
            state.searchValue = action.payload
        },
        setSort(state, action:PayloadAction<IFilterSliceState["sort"]>){
            state.sort = action.payload
        },
        setCurrentPage(state, action:PayloadAction<IFilterSliceState["currentPage"]>){
            state.currentPage = action.payload
        },
        setFilters(state, action:PayloadAction<IFilterSliceState>){
            if (Object.keys(action.payload).length){
                state.currentPage = Number(action.payload.currentPage)
                state.sort = action.payload.sort
                state.categoryId = Number(action.payload.categoryId)
            }else {
                state.currentPage = 1;
                state.categoryId = 0;
                state.sort = {
                    name: 'популярности',
                    sortProperty:ESortProperty.RATING_DESC
                }
            }

        }
    }
})




export const {setCategoryId, setSort,setCurrentPage,setFilters, setSearchValue} = filterSlice.actions
export default filterSlice.reducer
