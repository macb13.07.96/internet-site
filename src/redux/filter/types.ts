//interface filter
export enum ESortProperty {
    RATING_DESC = 'rating',
    RATING_ASC = '-rating',
    TITLE_DESC = 'title',
    TITLE_ASC = '-title',
    PRICE_DESC = 'price',
    PRICE_ASC = '-price',

}
export interface IFilterSort {
    name: string,
    sortProperty: ESortProperty
}


export interface IFilterSliceState {
    searchValue: string
    categoryId: number
    currentPage: number
    sort: IFilterSort,
}

export const initialState: IFilterSliceState = {
    searchValue: '',
    categoryId: 0,
    currentPage: 1,
    sort: {
        name: 'популярности',
        sortProperty: ESortProperty.RATING_DESC
    }
}

