import {CalcTotalPrice} from "./calcTotalPrice";
import {ICartItemsArr} from "../redux/cart/types";

export const GetCartFromLS = () => {
    const data = localStorage.getItem('cart')
    const items = data ? JSON.parse(data) : [];
    const totalPrice = CalcTotalPrice(items)

    return {
        items: items as ICartItemsArr[] ,
        totalPrice
    }


}