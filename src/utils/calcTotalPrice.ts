import {ICartItemsArr} from "../redux/cart/types";

export const CalcTotalPrice = (items:ICartItemsArr[]) => {
    return items.reduce((sum, obj) =>  obj.price * obj.count + sum, 0)
}