export interface IPizzaBlockProps {
    id: string;
    title: string;
    description: string;
    price: number[];
    imageUrl: string;
    sizes: number[];
    types: number[];

}
