export interface ISkeletonProps {
    speed?:number
    width?:number
    height?:number
    viewBox?:string
    backgroundColor?:string
    foregroundColor?:string
}