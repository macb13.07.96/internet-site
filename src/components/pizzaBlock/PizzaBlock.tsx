import React, {useState} from "react";
import {IPizzaBlockProps} from "./types";
import './_pizza-block.scss'
import {useDispatch, useSelector} from "react-redux";
import {Link} from "react-router-dom";
import {selectCertItemById} from "../../redux/cart/selectors";
import {ICartItemsArr} from "../../redux/cart/types";
import {addItem} from "../../redux/cart/slice";
import {PizzaModal} from "./pizzaModal/PizzaModal";
import {Modal} from "../modal/Modal";

const typeNames = ['тонкое', 'традиционное']

export const PizzaBlock: React.FC<IPizzaBlockProps> = (props) => {
    const dispatch = useDispatch()
    const cartItem = useSelector(selectCertItemById(props.id))
    const [activeType, setActiveType] = useState(0)
    const [activeSize, setActiveSize] = useState(0)
    const [activeModal, setActiveModal] = useState(false)
    const addCount = cartItem ? cartItem.count : 0

    const onClickAdd = () => {
        const item:ICartItemsArr = {
            id:props.id,
            title:props.title,
            price:props.price[activeSize],
            imageUrl:props.imageUrl,
            type:typeNames[activeType],
            size:props.sizes[activeSize],
            count: 0
        }
        dispatch(addItem(item))
    }
    return (
        <div className='pizza-block-wrapper'>
            <div className="pizza-block">
                <div key={props.id} onClick={() => setActiveModal(true)}>
                    <img className="pizza-block__image"
                         src={props.imageUrl}
                         alt="Pizza"/>
                    <h4 className="pizza-block__title">{props.title}</h4>
                </div>

                <Modal
                active = {activeModal}
                setActive = {setActiveModal}
                >
                    <div className='pizza-block-modal'>
                        <div className='pizza-block-modal__left'>
                            <h1>
                                {props.title}
                            </h1>
                            <span>
                                {props.description}
                            </span>
                        </div>
                        <div className='pizza-block-modal__right'>
                            <img className="pizza-block__image"
                                 src={props.imageUrl}
                                 alt="Pizza"/>
                        </div>
                    </div>
                </Modal>

                <div className="pizza-block__selector">
                    <ul>
                        {
                            props.types.map(( typeId) => (
                                <li onClick={() =>
                                    setActiveType(typeId)}
                                    className={activeType === typeId ? 'active' : ''}
                                    key={typeId}
                                >
                                    {typeNames[typeId]}
                                </li>

                            ))
                        }
                    </ul>
                    <ul>
                        {
                            props.sizes.map((size, index) => (
                                <li onClick={() =>
                                    setActiveSize(index)}
                                    className={activeSize === index ? 'active' : ''}
                                    key={size}
                                >
                                    {size} см.
                                </li>
                            ))
                        }
                    </ul>
                </div>
                <div className="pizza-block__bottom">

                    <div className="pizza-block__price">{props.price[activeSize]} ₽</div>
                    <button onClick={onClickAdd} className="button button--outline button--add">
                        <svg
                            width="12"
                            height="12"
                            viewBox="0 0 12 12"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg">
                            <path
                                d="M10.8 4.8H7.2V1.2C7.2 0.5373 6.6627 0 6 0C5.3373 0 4.8 0.5373 4.8 1.2V4.8H1.2C0.5373 4.8 0 5.3373 0 6C0 6.6627 0.5373 7.2 1.2 7.2H4.8V10.8C4.8 11.4627 5.3373 12 6 12C6.6627 12 7.2 11.4627 7.2 10.8V7.2H10.8C11.4627 7.2 12 6.6627 12 6C12 5.3373 11.4627 4.8 10.8 4.8Z"
                                fill="white"></path>
                        </svg>

                        <span>Добавить</span>
                        {addCount > 0 && <i>{addCount}</i>}
                    </button>
                </div>
            </div>
        </div>
    )
}
