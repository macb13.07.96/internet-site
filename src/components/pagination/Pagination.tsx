import React from "react";
import styles from './Pagination.module.scss'
import ReactPaginate from "react-paginate";
import {IPropsPagination} from "./types";
import {useSelector} from "react-redux";
import {selectPizzaData} from "../../redux/pizza/selectors";


export const Pagination: React.FC<IPropsPagination> = (props) => {
    const {items} = useSelector(selectPizzaData)

    return (
        <ReactPaginate
            className={styles.root}
            breakLabel='...'
            nextLabel='>'
            previousLabel='<'
            onPageChange={(event) => props.onChangePage(event.selected + 1)}
            pageRangeDisplayed={4}
            pageCount={Math.round(items.length / 4)}
            forcePage={props.currentPage - 1}
        />
    )
}
