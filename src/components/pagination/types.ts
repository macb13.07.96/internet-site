export interface IPropsPagination{
    currentPage: number
    onChangePage:(page:number) => void
}