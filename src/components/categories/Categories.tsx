import React, {memo} from "react";
import {IPropsCategories} from "./types";


const categories = [
    'Все',
    'Мясные',
    'Вегетарианская',
    'Гриль',
    'Острые',
    'Закрытые',
]

//memo - нужен для того чтоб не было лишних перересовок
export const Categories:React.FC<IPropsCategories> = memo((props) => {

    return (
        <div className="categories">
            <ul>
                {
                    categories.map((categoryName, index) => (
                        <li onClick={() =>
                            props.onChangeCategory(index)}
                            className={props.value === index ? 'active' : ''}
                            key={index}
                        >
                            {categoryName}
                        </li>
                    ))
                }
            </ul>
        </div>
    )
})