import React, {useCallback, useRef, useState} from "react";
import styles from './Search.module.scss'
import searchIcon from '../../assets/images/search.svg'
import clearIcon from './../../assets/images/delete.svg'
import debounce from 'lodash.debounce'
import {useDispatch} from "react-redux";
import {setSearchValue} from "../../redux/filter/slice";

export const Search:React.FC = () => {
    const dispatch = useDispatch()
    const [value, setValue] = useState('')
    const inputRef = useRef<HTMLInputElement>(null)
    const onClickClear = () => {
        dispatch(setSearchValue(''))
        setValue('');
        inputRef.current?.focus()

    }
    const updateSearchValue = useCallback(
        debounce((str:string) => {
            dispatch(setSearchValue(str))
        }, 250),
        [],
    )
    const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.target.value);
        updateSearchValue(event.target.value)
    }



    return (
        <div className={styles.root}>
            <img
                className={styles.icon}
                src={searchIcon}
                alt='Поиск'
            />
            <input
                ref={inputRef}
                value={value}
                onChange={onChangeInput}
                className={styles.input}
                placeholder="Поиск пиццы...."
            />
            {value
                && // условие если в в поиске ничего нет то отображаеться без иконки удаления если там чтото ввели то появиться значок удаления
                <img
                    onClick={onClickClear}
                    src={clearIcon}
                    className={styles.clearIcon}
                    alt="Удаление"
                />
            }


        </div>
    )
}
