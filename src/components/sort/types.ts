import {ESortProperty} from "../../redux/filter/types";

export type TListSort = {
    name:string;
    sortProperty: ESortProperty
}

export type TSortProps = {
    value: TListSort
}


