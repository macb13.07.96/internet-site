import React, {useCallback, useEffect} from "react";
import {Pagination, PizzaBlock, Skeleton, Sort, Categories} from "../components";
import {useSelector} from "react-redux";
import {setCategoryId, setCurrentPage} from "../redux/filter/slice";
import {useAppDispatch} from "../redux/store";
import {selectFilter} from "../redux/filter/selectors";
import {selectPizzaData} from "../redux/pizza/selectors";
import {fetchPizzas} from "../redux/pizza/asyncActions";

export const Home: React.FC = () => {
    const dispatch = useAppDispatch()
    const {categoryId, sort, currentPage, searchValue} = useSelector(selectFilter)
    const {items, status} = useSelector(selectPizzaData)
    const onChangeCategory = useCallback((idx: number) => {
        dispatch(setCategoryId(idx))
    }, [])
    const onChangePage = (page: number) => {
        dispatch(setCurrentPage(page))
    }

    const getPizzas = async () => {
        const order = sort.sortProperty.includes('-') ? 'asc' : 'desc';
        const sortBy = sort.sortProperty.replace('-', '');
        const category = categoryId > 0 ? String(categoryId) : '';
        const search = searchValue; //поисковик

        dispatch(
            fetchPizzas({
                sortBy,
                order,
                category,
                search,
                currentPage: String(currentPage),
            }),
        );

        window.scrollTo(0, 0)
    }

    useEffect(() => {
        getPizzas()
    }, [categoryId, sort.sortProperty, searchValue, currentPage])

    const pizzas = items.map((obj: any) => <PizzaBlock key={obj.id} {...obj}/>)
    const skeletons = [...new Array(4)].map((_, index) => <Skeleton key={index}/>)
    return (
        <div className='container'>
            <div className="content__top">
                <Categories value={categoryId} onChangeCategory={(index) => onChangeCategory(index)}/>
                <Sort value={sort}/>
            </div>
            <h2 className="content__title">Все пиццы</h2>
            {
                status === 'error' ? (
                    <div className='content__error-info'>
                        <h2>Произошла ошибка 😕</h2>
                        <p>
                            Не удалось получить пиццы.
                        </p>
                    </div>
                ) : (<div className="content__items">
                    {
                        status === 'loading'
                            ?
                            skeletons
                            :
                            pizzas
                    }
                </div>)
            }

            <Pagination currentPage={currentPage} onChangePage={onChangePage}/>
        </div>
    )
}
