import React from "react";
import {Header} from "../components/header/Header";
import {Outlet} from "react-router";


export const MainLayout:React.FC = () => {
    return (
        <div className="wrapper">
            <Header/>
            <div className="content">
                <Outlet/>
            </div>
        </div>
    )
}